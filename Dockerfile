FROM docker
RUN  mkdir -p ~/.docker/cli-plugins/ && \
     wget -O ~/.docker/cli-plugins/docker-buildx https://github.com/docker/buildx/releases/download/v0.5.1/buildx-v0.5.1.linux-amd64 && \
     chmod +x ~/.docker/cli-plugins/docker-buildx
